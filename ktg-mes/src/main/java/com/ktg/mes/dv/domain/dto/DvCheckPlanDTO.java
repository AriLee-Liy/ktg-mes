package com.ktg.mes.dv.domain.dto;

import lombok.Data;

@Data
public class DvCheckPlanDTO {

    private String planType;

    private String machineryCode;

}
