package com.ktg.mes.dv.domain.dto;

import lombok.Data;

@Data
public class DvRepairDTO {

    private String machineryCode;

}
