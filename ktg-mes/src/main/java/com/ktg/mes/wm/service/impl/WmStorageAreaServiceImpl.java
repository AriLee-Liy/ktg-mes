package com.ktg.mes.wm.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ktg.common.core.domain.AjaxResult;
import com.ktg.common.utils.DateUtils;
import com.ktg.mes.wm.domain.WmMaterialStock;
import com.ktg.mes.wm.service.IWmMaterialStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ktg.mes.wm.mapper.WmStorageAreaMapper;
import com.ktg.mes.wm.domain.WmStorageArea;
import com.ktg.mes.wm.service.IWmStorageAreaService;

/**
 * 库位设置Service业务层处理
 * 
 * @author yinjinlu
 * @date 2022-05-08
 */
@Service
public class WmStorageAreaServiceImpl implements IWmStorageAreaService 
{
    @Autowired
    private WmStorageAreaMapper wmStorageAreaMapper;

    @Autowired
    private IWmMaterialStockService wmMaterialStockService;

    /**
     * 查询库位设置
     * 
     * @param areaId 库位设置主键
     * @return 库位设置
     */
    @Override
    public WmStorageArea selectWmStorageAreaByAreaId(Long areaId)
    {
        return wmStorageAreaMapper.selectWmStorageAreaByAreaId(areaId);
    }

    @Override
    public WmStorageArea selectWmStorageAreaByAreaCode(String areaCode) {
        return wmStorageAreaMapper.selectWmStorageAreaByAreaCode(areaCode);
    }

    /**
     * 查询库位设置列表
     * 
     * @param wmStorageArea 库位设置
     * @return 库位设置
     */
    @Override
    public List<WmStorageArea> selectWmStorageAreaList(WmStorageArea wmStorageArea)
    {
        return wmStorageAreaMapper.selectWmStorageAreaList(wmStorageArea);
    }

    /**
     * 新增库位设置
     * 
     * @param wmStorageArea 库位设置
     * @return 结果
     */
    @Override
    public int insertWmStorageArea(WmStorageArea wmStorageArea)
    {
        wmStorageArea.setCreateTime(DateUtils.getNowDate());
        return wmStorageAreaMapper.insertWmStorageArea(wmStorageArea);
    }

    /**
     * 修改库位设置
     * 
     * @param wmStorageArea 库位设置
     * @return 结果
     */
    @Override
    public int updateWmStorageArea(WmStorageArea wmStorageArea)
    {
        wmStorageArea.setUpdateTime(DateUtils.getNowDate());
        return wmStorageAreaMapper.updateWmStorageArea(wmStorageArea);
    }

    @Override
    public void updateWmStorageAreaProductMixing(Long locationId, String flag) {
        WmStorageArea area = new WmStorageArea();
        area.setLocationId(locationId);
        area.setProductMixing(flag);
        wmStorageAreaMapper.updateWmStorageAreaProductMixing(area);
    }

    @Override
    public void updateWmStorageAreaBatchMixing(Long locationId, String flag) {
        WmStorageArea area = new WmStorageArea();
        area.setLocationId(locationId);
        area.setBatchMixing(flag);
        wmStorageAreaMapper.updateWmStorageAreaBatchMixing(area);
    }

    /**
     * 批量删除库位设置
     *
     * @param areaIds 需要删除的库位设置主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteWmStorageAreaByAreaIds(Long[] areaIds)
    {
        // 查询所有删除数据
        List<WmStorageArea> areaList = wmStorageAreaMapper.selectByAreaIds(areaIds);
        for (WmStorageArea item : areaList) {
            String areaCode = item.getAreaCode();
            if (areaCode.contains("VIRTUAL")) {
                return AjaxResult.error("虚拟库位不能删除");
            }
        }

        // 校验库位中是否存在物料
        for (Long areaId : areaIds) {
            // 根据库位查询相关数据
            List<WmMaterialStock> list = wmMaterialStockService.getByAreaId(areaId);
            if (list != null && list.size() > 0) {
                Map<Long, List<WmMaterialStock>> collect = list.stream()
                        .collect(Collectors.groupingBy(WmMaterialStock::getItemId));
                for (Long l : collect.keySet()) {
                    List<WmMaterialStock> wmMaterialStocks = collect.get(l);
                    BigDecimal reduce = wmMaterialStocks.stream().map(WmMaterialStock::getQuantityOnhand).reduce(BigDecimal.ZERO, BigDecimal::add);
                    if (!(reduce.compareTo(BigDecimal.ZERO) == 0)) {
                        return AjaxResult.error("库位中还有库存不能删除");
                    }
                }
            }
        }
        return AjaxResult.success(wmStorageAreaMapper.deleteWmStorageAreaByAreaIds(areaIds));
    }

    /**
     * 删除库位设置信息
     * 
     * @param areaId 库位设置主键
     * @return 结果
     */
    @Override
    public int deleteWmStorageAreaByAreaId(Long areaId)
    {
        return wmStorageAreaMapper.deleteWmStorageAreaByAreaId(areaId);
    }

    @Override
    public int deleteByWarehouseId(Long warehouseId) {
        return wmStorageAreaMapper.deleteByWarehouseId(warehouseId);
    }

    @Override
    public int deleteByLocationId(Long locationId) {
        return wmStorageAreaMapper.deleteByLocationId(locationId);
    }

    @Override
    public int deleteByLocationIds(Long[] locationIds) {
        return wmStorageAreaMapper.deleteByLocationIds(locationIds);
    }
}
